#include <stdbool.h> // bool, true, false
#include <stdio.h>  // printf, scanf
#include <stdlib.h> // malloc

#include "pacman.h"

itemtree itemtree_creer(char elt, itemtree, itemtree, itemtree, itemtree);
void parcours_prefixe(itemtree, char **, int *);
int taille(itemtree);

int main(){
	int k = 0;
	itemtree tree = itemtree_creer('*',0,0,0,0);
	char * tab = malloc(taille(tree)*sizeof(char));
	parcours_prefixe(tree,&tab,&k);

	for(int i=0;i<taille(tree);i++){
		printf("%c", tab[i]);
	}
	printf("\n");
	free(tab);
}


itemtree itemtree_creer(char elt, itemtree ntree, itemtree etree, itemtree stree, itemtree wtree){
	itemtree tree = malloc(sizeof(struct itemnode));
	if(tree!=NULL){
		tree->item = elt;
		tree->n = ntree;
		tree->e = etree;
		tree->s = stree;
		tree->w = wtree;
	}
	return tree;
}

void parcours_prefixe(itemtree tree, char ** ptab, int * k){
	if(tree == NULL){
		return;
	}
	//printf("%c ",tree->item);
	(*ptab)[(*k)++] = tree->item;
	parcours_prefixe(tree->n,ptab,k);
	parcours_prefixe(tree->e,ptab,k);
	parcours_prefixe(tree->s,ptab,k);
	parcours_prefixe(tree->w,ptab,k);
}

int taille(itemtree tree){
	if(tree==NULL) return 0;
	else return 1 + taille(tree->n) + taille(tree->e) + taille(tree->s) + taille(tree->w);
}
