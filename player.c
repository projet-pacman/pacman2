// add the needed C libraries below
#include <stdbool.h> // bool, true, false
#include <stdlib.h> // rand
#include <stdio.h>  // printf, scanf

// look at the file below for the definition of the direction type
// pacman.h must not be modified!
#include "pacman.h"

// ascii characters used for drawing levels
extern const char PACMAN; // ascii used for pacman
extern const char WALL; // ascii used for the walls
extern const char PATH; // ascii used for the explored paths
extern const char DOOR; // ascii used for the ghosts' door
extern const char VIRGIN_PATH; // ascii used for the unexplored paths
extern const char ENERGY; // ascii used for the energizers
extern const char GHOST1; // ascii used for the ghost 1
extern const char GHOST2; // ascii used for the ghost 2
extern const char GHOST3; // ascii used for the ghost 3
extern const char GHOST4; // ascii used for the ghost 4

// reward (in points) when eating dots/energizers 
extern const int VIRGIN_PATH_SCORE; // reward for eating a dot
extern const int ENERGY_SCORE; // reward for eating an energizer

// put the student names below (mandatory)
const char * binome="ABGRALL";

// Structure contenant des données sur les fantômes
struct ghosts {
	char who; // nom du fantôme (GHOST1,...,GHOST4)
	char dir; // direction dans laquelle se trouve le fantôme
			  // il y a 8 choix posibles : 'n', 'e', 's' et 'w' pour les directions standard;
			  // '1', '2', '3' et '4' pour les directions mixtes, ie. respectivement
			  //  NW,  NE,  SW et  SE (Nord-Ouest, Nord-Est, Sud-Ouest et Sud-Est)
	int dist; // distance à Pacman
};
typedef struct ghosts ghost;

// put the prototypes of your additional functions/procedures below
int max(int,int);
int min(int,int);
int hauteur(itemtree);
void coins(itemtree, itemtree, itemtree, itemtree, char*, char*, char*, char*);
void print_tree(itemtree);
int taille(itemtree);
void parcours_prefixe(itemtree, char **, int *);
int distance(itemtree, char);
int distanceV2(itemtree, char, char);
void init_GhostTab(itemtree, itemtree, itemtree, itemtree, char, char, char, char, ghost **);
void fill_GhostTab(itemtree, itemtree, itemtree, itemtree, char, char, char, char, int, ghost **);
void init_parcours(itemtree, itemtree, itemtree, itemtree, char **, char **, char **, char **);
void recherche_points(itemtree, itemtree, itemtree, itemtree, char *, char *, char *, char *, bool *, char *, bool);
direction char_vers_direction(itemtree, itemtree, itemtree, itemtree, char);
bool detecte_egalite(int, int, int, int, int);
direction fuir(itemtree, itemtree, itemtree, itemtree, ghost *);
void detection_danger(itemtree, itemtree, itemtree, itemtree, int, direction *, ghost *);
void echappatoire(char, direction *, itemtree, itemtree, itemtree, itemtree);
bool estValide(itemtree);
bool presence_fantome(itemtree);
direction attaquer(itemtree, itemtree, itemtree, itemtree, ghost *);
direction explorer(itemtree, itemtree, itemtree, itemtree, direction);
bool oscillation(direction, direction);

// change the pacman function below to build your own player
// your new pacman function can use as many additional functions/procedures as needed; put the code of these functions/procedures *AFTER* the pacman function
direction pacman(
		dirtree treemap, // tree of the moves made since the beginning of the game (see pacman.h for the dirtree type); the direction value of the root node is set to -1
		dirtree treepos, // points to the dirtree treemap node in which pacman currently stands
		itemtree northtree, // tree of the items pacman sees when looking in the north direction (see pacman.h for the itemtree type)  
		itemtree easttree, // tree of the items pacman sees when looking in the east direction (see pacman.h for the itemtree type)
		itemtree southtree, // tree of the items pacman sees when looking in the south direction (see pacman.h for the itemtree type)
		itemtree westtree, // tree of the items pacman sees when looking in the west direction (see pacman.h for the itemtree type)
		direction lastdirection, // last move made by pacman (see pacman.h for the direction type; lastdirection value is -1 at the beginning of the game
		bool energy, // is pacman in energy mode? 
		int remainingenergymoderounds // number of remaining rounds in energy mode, if energy mode is true
		) {
	direction d; // the direction to return
  
	// put your code here

	bool PasDePoints = false; // indique s'il n'y a aucun point à récupérer dans le champ de vision de Pacman; non par défaut 

	char NordEst = '?'; // nature de la case au Nord-Est de Pacman; '?' par défaut
	char NordOuest = '?'; // nature de la case au Nord-Est de Pacman; '?' par défaut
	char SudEst = '?'; // nature de la case au Sud-Est de Pacman; '?' par défaut
	char SudOuest = '?'; // nature de la case au Sud-Ouest de Pacman; '?' par défaut

	char * parcoursNord = NULL; // pointeur sur tableau dynamique de caractères stockant le parcours préfixe de l'arbre northtree
	char * parcoursEst = NULL; // pointeur sur tableau dynamique de caractères stockant le parcours préfixe de l'arbre easttree
	char * parcoursSud = NULL; // pointeur sur tableau dynamique de caractères stockant le parcours préfixe de l'arbre southtree
	char * parcoursOuest = NULL; // pointeur sur tableau dynamique de caractères stockant le parcours préfixe de l'arbre westtree

	char dirPoints = '?'; // direction à prendre pour aller dans la direction avec le plus de points; '?' par défaut

	ghost * GhostTab = malloc(4*sizeof(ghost)); // tableau dynamique de type ghost contenant des informations sur les fantômes

	/* Analyse de l'environnement de Pacman */

	// détermination de la nature des blocs dans les directions mixtes
	// (Nord-Est, Nord-Ouest, Sud-Est, Sud-Ouest)
	coins(northtree,easttree,southtree,westtree,&NordEst,&NordOuest,&SudEst,&SudOuest);

	// initialisation et complétion de GhostTab
	init_GhostTab(northtree,easttree,southtree,westtree,NordEst,NordOuest,SudEst,SudOuest,&GhostTab);

	// parcours préfixe de chaque arbre de direction et stockage des valeurs rencontrées
	// dans les 4 tableaux dynamiques de caractères parcoursN, parcoursE, parcoursS et parcoursW
	init_parcours(northtree,easttree,southtree,westtree,&parcoursNord,&parcoursEst,&parcoursSud,&parcoursOuest);

	// comptage du nombre de points selon chaque direction
	recherche_points(northtree,easttree,southtree,westtree,parcoursNord,parcoursEst,parcoursSud,parcoursOuest,&PasDePoints,&dirPoints,energy);


	/* Algorithme de prise de décision */

	bool fantome_proche =    (GhostTab[0].dist < 4 && GhostTab[0].dist >= 0) 
						  || (GhostTab[1].dist < 4 && GhostTab[1].dist >= 0) 
						  || (GhostTab[2].dist < 4 && GhostTab[2].dist >= 0) 
						  || (GhostTab[3].dist < 4 && GhostTab[3].dist >= 0);


	// manger les fantômes si il reste assez de tours en mode énergie
	if(energy && remainingenergymoderounds > 3 && fantome_proche){
		d = attaquer(northtree,easttree,southtree,westtree,GhostTab);
	}

	// si un fantôme est à moins de 3 cases de Pacman, partir dans une autre direction
	else if(fantome_proche){
		d = fuir(northtree,easttree,southtree,westtree,GhostTab);
	}

	// s'il n'y a pas de points (ni d'énergie) dans le champ de vision de Pacman
	// on explore la carte le plus possible pour en trouver
	else if(PasDePoints){
		d = explorer(northtree,easttree,southtree,westtree,lastdirection);
	}

	// sinon, aller dans la direction où il y a le plus de points à récupérer
	else{
		d = char_vers_direction(northtree,easttree,southtree,westtree,dirPoints);
	}

	// libération des différents pointeurs sur tableaux
	free(parcoursNord);
	free(parcoursEst);
	free(parcoursSud);
	free(parcoursOuest);
	free(GhostTab);

  	// answer to the game engine 
	return d;
}

// the code of your additional functions/procedures must be put below


/* Fonction max

Renvoit le maximum de deux entiers donnés.

Paramètres : 
	int x : un des deux entiers à comparer;
	int y : l'autre entier à comparer.
Retourne :
	int : l'entier le plus grand entre x et y.

*/

int max(int x, int y){
    if(x > y){
        return x;
    } else{
        return y;
    }
}


/* Fonction min

Renvoit le minimum de deux entiers donnés.

Paramètres : 
	int x : un des deux entiers à comparer;
	int y : l'autre entier à comparer.
Retourne :
	int : l'entier le plus petit entre x et y.

*/

int min(int x, int y){
	if(x < y){
        return x;
    } else{
    	return y;
   	}
}


/********************************/
/* Modules utiles sur les arbres*/
/********************************/


/* Fonction hauteur

Calcule la hauteur, ie. la profondeur maximum, de l'arbre itemtree fourni en paramètre.

Paramètres :
	itemtree tree : l'arbre dont on veut calculer la hauteur.
Retourne :
	int : la hauteur de l'arbre entré.

*/

int hauteur(itemtree tree){
	if(tree == NULL) return 0;
	else return 1 + max(max(hauteur(tree->n),hauteur(tree->e)),max(hauteur(tree->s),hauteur(tree->w)));
}


/* Fonction taille

Calcule la taille, ie. le nombre de noeuds, de l'arbre itemtree fourni en paramètre.

Paramètres :
	itemtree tree : l'arbre dont on veut calculer la taille.
Retourne :
	int : la taille de l'arbre entré.

*/

int taille(itemtree tree){
	if(tree==NULL) return 0;
	else return 1 + taille(tree->n) + taille(tree->e) + taille(tree->s) + taille(tree->w);
}


/* Procédure parcours_prefixe 

Effectue le parcours préfixe de l'arbre itemtree fourni en paramètre et stocke les éléments
parcourus dans un tableau dynamique.

Paramètres :
	itemtree tree : l'arbre dont on veut effectuer le parcours préfixe;
	char ** ptab : un pointeur sur tableau dynamique de caractères. A la fin de la procédure,
				   il contiendra les éléments de l'arbre issus de son parcours préfixe;
	int * pk : un pointeur sur entier, qui nous sert à manipuler le tableau *ptab. Il est
			   initialisé à 0 en dehors de la procédure car, celle-ci étant récursive, s'il était
			   initialisé à l'intérieur, il serait remis à 0 à chaque appel récursif.
*/

void parcours_prefixe(itemtree tree, char ** ptab, int * pk){
	if(tree == NULL){
		return;
	}

	(*ptab)[(*pk)++] = tree->item; // incrémentation de k après l'affectation
								   // du caractère lu dans *ptab
	parcours_prefixe(tree->n,ptab,pk);
	parcours_prefixe(tree->e,ptab,pk);
	parcours_prefixe(tree->s,ptab,pk);
	parcours_prefixe(tree->w,ptab,pk);
}


/* Procédure print_tree 

Reconstitue l'arbre fourni en paramètre et l'affiche dans le terminal.

Paramètres :
	itemtree tree : l'arbre que l'on veut afficher dans le terminal.

*/

void print_tree(itemtree tree){
	if(tree==NULL){ /* Base : affichage de l'arbre vide */
    printf("0");
    } 
	else{ /* Recurrence */
    	/* on affiche l'etiquette */ 
    	printf("<");
    	printf("%c", tree->item);
		printf(",");
		/* on affiche au Nord */
		print_tree(tree->n);
		printf(",");
		/* on affiche à l'Est */
		print_tree(tree->e);
		printf(",");
		/* on affiche au Sud */
		print_tree(tree->s);
		printf(",");
		/* on affiche à l'Ouest*/
		print_tree(tree->w);
		printf(">");
	} 
}

/*------------------------------------------------------------------------------------------------*/


/* Procédure coins 

Procédure en charge de déterminer la nature des blocs dans les directions mixtes (Nord-Ouest, 
Nord-Est, Sud-Ouest et Sud-Est) par rapport à l'emplacement actuel de Pacman, et de mettre à jour
les caractères lus dans ces directions, si possible.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char * NE : un pointeur sur caractère chargé de mettre à jour le caractère lu au Nord-Est NE;
	char * NW : un pointeur sur caractère chargé de mettre à jour le caractère lu au Nord-Ouest NW;
	char * SE : un pointeur sur caractère chargé de mettre à jour le caractère lu au Sud-Est SE;
	char * SW : un pointeur sur caractère chargé de mettre à jour le caractère lu au Sud-Ouest SW.

*/

void coins(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char * NE, char * NW, char * SE, char * SW){

	// cette garde sert à éviter une erreur de segmentation si l'arbre entré est réduit à la racine
	if(hauteur(etree)>1){ 
		*NE = etree->n->item;
		*SE = etree->s->item;
	}

	if(hauteur(wtree)>1){
		*NW = wtree->n->item;
		*SW = wtree->s->item;
	}

	// si les arbres de direction Est et Ouest n'ont pas pu lire les éléments dans les directions 
	// mixtes, on essaie avec les arbres de direction Nord et Sud pour être sûr d'avoir bien regardé
	if(hauteur(ntree)>1){ 
		*NE = ntree->e->item;
		*NW = ntree->w->item;
	}

	if(hauteur(stree)>1){ 
		*SE = stree->e->item;
		*SW = stree->w->item;
	}
}


/* Fonction distance

Calcule la distance entre Pacman et un caractère donné dans un arbre fourni en paramètre.

Paramètres :
	itemtree tree : l'arbre dans lequel on effectue la recherche;
	char elt : l'élement recherché dans l'arbre.
Retourne :
	int : la distance entre Pacman et le caractère recherché.

*/

int distance(itemtree tree, char elt) {
	if(tree==NULL) return -1;
	if(tree->item==elt) return 1; 
	// on convient que la distance entre la racine de tree et elle-même est 1, car 
	// ce qui nous intéresse est la distance entre Pacman et elt, pas entre la racine de tree et elt

	int nord = distance(tree->n,elt);
	if(nord!=-1) return 1+nord;

	int est = distance(tree->e,elt);
	if(est!=-1) return 1+est;

	int sud = distance(tree->s,elt);
	if(sud!=-1) return 1+sud;

	int ouest = distance(tree->w,elt);
	if(ouest!=-1) return 1+ouest;

	// si on n'a pas trouvé
	return -1;
}


/* Fonction distanceV2

Calcule la distance entre Pacman et un caractère donné dans un arbre fourni en paramètre, mais
uniquement dans un des quatre sous-arbres (n,e,s ou w) de l'arbre fourni, cela étant imposé
par le choix du caractère point_cardinal.

Paramètres :
	itemtree tree : l'arbre dans lequel on effectue la recherche;
	char elt : l'élement recherché dans l'arbre;
	char point_cardinal : un caractère prenant la valeur 'n','e','s,' ou 'w', indiquant dans quel
						  sous-arbre de tree effectuer la recherche.
Retourne :
	int : la distance entre Pacman et le caractère recherché dans un des sous-arbres
		  de l'arbre tree fourni en paramètre.

*/

int distanceV2(itemtree tree, char elt, char point_cardinal) {
	int dist_tmp; //distance mise à jour à chaque appel récursif 
	if(tree==NULL) return -1;
	if(tree->item==elt) return 1; 

	if(point_cardinal == 'n'){
		dist_tmp = distanceV2(tree->n,elt,point_cardinal);
		if(dist_tmp!=-1) return 1+dist_tmp;
	}

	if(point_cardinal == 'e'){
		dist_tmp = distanceV2(tree->e,elt,point_cardinal);
		if(dist_tmp!=-1) return 1+dist_tmp;
	}

	if(point_cardinal == 's'){
		dist_tmp = distanceV2(tree->s,elt,point_cardinal);
		if(dist_tmp!=-1) return 1+dist_tmp;
	}

	if(point_cardinal == 'w'){
		dist_tmp = distanceV2(tree->w,elt,point_cardinal);
		if(dist_tmp!=-1) return 1+dist_tmp;
	}

	// si on n'a pas trouvé
	return -1;
}


/* Procédure init_GhostTab

Initialise le tableau dynamique GhostTab du programme principal : pour chaque fantôme, met à jour
les champs .who, .dir et .dist (ces deux derniers sont mis à jour grâce au module
fill_GhostTab) selon les informations obtenues à l'appel de cette procédure.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char NE : caractère indiquant la nature de la case au Nord-Est de Pacman;
	char NW : caractère indiquant la nature de la case au Nord-Ouest de Pacman;
	char SE : caractère indiquant la nature de la case au Sud-Est de Pacman;
	char SW : caractère indiquant la nature de la case au Sud-Ouest de Pacman;
	ghost ** pGhostTab : pointeur sur tableau dynamique de type ghost. C'est via lui qu'on met
						 à jour les différents champs du tableau GhostTab.

*/

void init_GhostTab(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char NE, char NW, char SE, char SW, ghost ** pGhostTab){

	(*pGhostTab)[0].who = GHOST1;
	(*pGhostTab)[1].who = GHOST2;
	(*pGhostTab)[2].who = GHOST3;
	(*pGhostTab)[3].who = GHOST4;

	// détermination d'informations sur GHOST1, 2, 3 et 4
	fill_GhostTab(ntree,etree,stree,wtree,NE,NW,SE,SW,1,pGhostTab);
	fill_GhostTab(ntree,etree,stree,wtree,NE,NW,SE,SW,2,pGhostTab);
	fill_GhostTab(ntree,etree,stree,wtree,NE,NW,SE,SW,3,pGhostTab);
	fill_GhostTab(ntree,etree,stree,wtree,NE,NW,SE,SW,4,pGhostTab);
}


/* Procédure fill_GhostTab 

Remplit les champs .dir et .dist de chaque élément ghost du tableau dynamique GhostTab 
dont un pointeur est fourni en paramètre.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char NE : caractère indiquant la nature de la case au Nord-Est de Pacman;
	char NW : caractère indiquant la nature de la case au Nord-Ouest de Pacman;
	char SE : caractère indiquant la nature de la case au Sud-Est de Pacman;
	char SW : caractère indiquant la nature de la case au Sud-Ouest de Pacman;
	int i : entier correspondant au numéro d'un des fantômes (1<=i<=4);
	ghost ** pGhostTab : pointeur sur tableau dynamique de type ghost. C'est via lui qu'on met
						 à jour les différents champs du tableau GhostTab.

*/

void fill_GhostTab(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char NE, char NW, char SE, char SW, int i, ghost ** pGhostTab){
	
	int ndist; // distance entre Pacman et fantome dans le couloir au Nord de Pacman
	int edist; // distance entre Pacman et fantome dans le couloir à l'Est de Pacman
	int sdist; // distance entre Pacman et fantome dans le couloir au Sud de Pacman
	int wdist; // distance entre Pacman et fantome dans le couloir à l'Ouest de Pacman
	char fantome; // prend la valeur GHOST1, GHOST2, GHOST3 ou GHOST4 selon la valeur de i correspondant

	switch(i){
		case 1: fantome = GHOST1; break;
		case 2: fantome = GHOST2; break;
		case 3: fantome = GHOST3; break;
		case 4: fantome = GHOST4; break;
	}

	ndist = distanceV2(ntree,fantome,'n');
	edist = distanceV2(etree,fantome,'e');
	sdist = distanceV2(stree,fantome,'s');
	wdist = distanceV2(wtree,fantome,'w');

	// on convient qu'un fantôme dans une direction mixte est à une distance de 1 de Pacman
	if(NE == fantome || NW == fantome || SE == fantome || SW == fantome) (*pGhostTab)[i-1].dist = 1;
	// on prend la plus grande valeur pour éliminer les éventuels "-1"
	// étant donné qu'un fantôme ne peut être qu'à un endroit à la fois
	else (*pGhostTab)[i-1].dist = max(max(ndist,edist),max(sdist,wdist));

	// si on a trouvé fantome dans la direction Nord
	if(ndist != -1) (*pGhostTab)[i-1].dir = 'n';

	// sinon si on a touvé fantôme à l'Est
	else if(edist != -1) (*pGhostTab)[i-1].dir = 'e';

	// sinon si on a touvé fantôme au Sud
	else if(sdist != -1) (*pGhostTab)[i-1].dir = 's';

	// sinon si on a touvé fantôme à l'Ouest
	else if(wdist != -1) (*pGhostTab)[i-1].dir = 'w';

	/* prise en compte des directions mixtes (Nord-Est, ...) */

	else if(NW == fantome) (*pGhostTab)[i-1].dir = '1';
	else if(NE == fantome) (*pGhostTab)[i-1].dir = '2';
	else if(SW == fantome) (*pGhostTab)[i-1].dir = '3';
	else if(SE == fantome) (*pGhostTab)[i-1].dir = '4';

	// si fantome n'a été trouvé dans aucune direction
	else (*pGhostTab)[i-1].dir = '?';
}


/* Procédure init_parcours 

Remplit 4 tableaux dynamiques de caractères, chacun stockant le parcours infixe d'un des 4 arbres
de direction fournis en paramètre.

Paramètres :  
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char ** pParcoursN : pointeur sur le tableau dynamique de caractères ParcoursN
	char ** pParcoursE : pointeur sur le tableau dynamique de caractères ParcoursE
	char ** pParcoursS : pointeur sur le tableau dynamique de caractères ParcoursS
	char ** pParcoursW : pointeur sur le tableau dynamique de caractères ParcoursW

*/

void init_parcours(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char ** pParcoursN, char ** pParcoursE, char ** pParcoursS, char ** pParcoursW){

	// entiers servants à indexer le tableau modifié au cours de la procédure parcours_préfixe
	int i = 0;
	int j = 0;
	int k = 0;
	int l = 0;

	// allocation de l'espace nécessaire pour stocker les parcours
	*pParcoursN = malloc(taille(ntree)*sizeof(char));
	*pParcoursE = malloc(taille(etree)*sizeof(char));
	*pParcoursS = malloc(taille(stree)*sizeof(char));
	*pParcoursW = malloc(taille(wtree)*sizeof(char));

	parcours_prefixe(ntree,pParcoursN,&i);
	parcours_prefixe(etree,pParcoursE,&j);
	parcours_prefixe(stree,pParcoursS,&k);
	parcours_prefixe(wtree,pParcoursW,&l);
}


/* Procédure recherche_points 

Parcourt les 4 tableaux dynamiques de caractères stockant les parcours préfixes des 4 arbres de 
direction, compte le nombre de cases VIRGIN_PATH et les énergies recontrées selon chaque direction.
Ensuite, produit un score pour chaque direction. La meilleure direction possible, ie. celle ayant
le plus haut score, est ensuite retournée au programme principal en mettant à jour la valeur
du pointeur sur caractère pDirPoints. Met également un booléen à jour si tous les scores sont nuls.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char * parcoursNord : tableau dynamique de caractères contenant le parcours préfixe de northtree;
	char * parcoursEst : tableau dynamique de caractères contenant le parcours préfixe de easttree;
	char * parcoursSud : tableau dynamique de caractères contenant le parcours préfixe de southtree;
	char * parcoursOuest : tableau dynamique de caractères contenant le parcours préfixe de westtree;
	bool * pPasDePoints : pointeur sur le booléen PasDePoints du programme principal, le met à jour
						  (à false) si aucun point n'a été rencontré dans aucun des tableaux;
	char * pDirPoints : pointeur sur le caractère dirPoints du programme principal, le met à jour
						à une des valeurs 'n','e','s' ou 'w' selon la direction rapportant le plus de points;
	bool energy : booléen indiquant si Pacman est en mode énergie ou non.

*/

void recherche_points(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char * parcoursN, char * parcoursE, char * parcoursS, char * parcoursW, bool * pPasDePoints, char * pDirPoints, bool energy){
	
	int i; // itérateur de boucle
	
	bool premier_point_N = true; // booléen chargé d'indiquer le premier point rencontré dans parcoursN
	bool premier_point_E = true; // booléen chargé d'indiquer le premier point rencontré dans parcoursE
	bool premier_point_S = true; // booléen chargé d'indiquer le premier point rencontré dans parcoursS
	bool premier_point_W = true; // booléen chargé d'indiquer le premier point rencontré dans parcoursW

	int i1 = 99; // entier chargé de stocker l'indice i de parcoursN correspondant au premier point qui y a été rencontré; 99 par défaut
	int i2 = 99; // entier chargé de stocker l'indice i de parcoursE correspondant au premier point qui y a été rencontré; 99 par défaut
	int i3 = 99; // entier chargé de stocker l'indice i de parcoursS correspondant au premier point qui y a été rencontré; 99 par défaut
	int i4 = 99; // entier chargé de stocker l'indice i de parcoursW correspondant au premier point qui y a été rencontré; 99 par défaut
	int minind; // le minimum de ces 4 indices

	int scoreN = 0; // entier stockant le score au fur et à mesure du parcours du tableau parcoursN; 0 par défaut
	int scoreE = 0; // entier stockant le score au fur et à mesure du parcours du tableau parcoursE; 0 par défaut
	int scoreS = 0; // entier stockant le score au fur et à mesure du parcours du tableau parcoursS; 0 par défaut
	int scoreW = 0; // entier stockant le score au fur et à mesure du parcours du tableau parcoursW; 0 par défaut
	int maxscore; // le maximum de ces 4 scores
	bool egalite = false; // booléen indiquant s'il y a une égalité entre deux scores sont égaux au maximum; faux par défaut

	int bonus_points = 20; // variable à changer délicatement pour changer la façon dont
						   // Pacman parcourt la carte pour chercher des points.
	int bonus_NRJ = 300;

	int tailleN = taille(ntree); // permet de savoir où s'arrêter dans la boucle parcourant parcoursN
	int tailleE = taille(etree); // permet de savoir où s'arrêter dans la boucle parcourant parcoursE
	int tailleS = taille(stree); // permet de savoir où s'arrêter dans la boucle parcourant parcoursS
	int tailleW = taille(wtree); // permet de savoir où s'arrêter dans la boucle parcourant parcoursW

	for(i=0;i<tailleN;i++){
		// on décrémente de i le score pour que Pacman se tienne à une même direction
		// et n'aille pas 3 cases à gauche, puis 4 à droite, puis 2 à gauche, ...
		if(parcoursN[i] == VIRGIN_PATH) scoreN = scoreN + bonus_points - i;
			if(parcoursN[i] == VIRGIN_PATH && premier_point_N) {
				i1 = i;
				premier_point_N = false;
			}
		// on augmente fortement le score si une énergie est visible (ici au Nord) et que Pacman
		// n'est pas déjà en mode énergie
		if(!energy && parcoursN[i] == ENERGY) scoreN = scoreN + bonus_NRJ;
	}

	for(i=0;i<tailleE;i++){
		if(parcoursE[i] == VIRGIN_PATH) scoreE = scoreE + bonus_points - i;
			if(parcoursE[i] == VIRGIN_PATH && premier_point_E) {
				i2 = i;
				premier_point_E = false;
			}
		if(!energy && parcoursE[i] == ENERGY) scoreE = scoreE + bonus_NRJ;
	}

	for(i=0;i<tailleS;i++){
		if(parcoursS[i] == VIRGIN_PATH) scoreS = scoreS + bonus_points - i;
			if(parcoursS[i] == VIRGIN_PATH && premier_point_S) {
				i3 = i;
				premier_point_S = false;
			}
		if(!energy && parcoursS[i] == ENERGY) scoreS = scoreS + bonus_NRJ;
	}

	for(i=0;i<tailleW;i++){
		if(parcoursW[i] == VIRGIN_PATH) scoreW = scoreW + bonus_points - i;
			if(parcoursW[i] == VIRGIN_PATH && premier_point_W) {
				i4 = i;
				premier_point_W = false;
			}
		if(!energy && parcoursW[i] == ENERGY) scoreW = scoreW + bonus_NRJ;
	}

	maxscore = max(max(scoreN,scoreE),max(scoreS,scoreW));
	minind = min(min(i1,i2),min(i3,i4));

	egalite = detecte_egalite(scoreN,scoreE,scoreS,scoreW,maxscore);

	// s'il y a au moins un point a récupérer
	if(maxscore != 0){
		
		// si le maximum est unique
		if(!egalite){

			if(maxscore==scoreN) *pDirPoints = 'n';
			else if(maxscore==scoreE) *pDirPoints = 'e';
			else if(maxscore==scoreS) *pDirPoints = 's';
			else if(maxscore==scoreW) *pDirPoints = 'w';
		}

		// s'il y a plusieurs maximums, prendre la direction dans laquelle les points sont les plus proches
		// ie. la direction correspondant à l'indice le plus petit trouvé dans les 4 tableaux de parcours
		else{
			if(minind==i1) *pDirPoints = 'n';
			else if(minind==i2) *pDirPoints = 'e';
			else if(minind==i3) *pDirPoints = 's';
			else if(minind==i4) *pDirPoints = 'w';
		}
	}

	// s'il n'y a pas de points visibles, on met à jour un booléen utile 
	// dans la prise de décisions du programme principal
	else *pPasDePoints = true;
}


/* Fonction detecte_egalite 

Motivation : dans la procédure recherche_points, s'il y a plusieurs maximums, Pacman va osciller 
entre les directions associées à ces maximums sans parvenir à en choisir une pour de bon.
Ainsi, cette fonction cherche si, parmi 5 entiers donnés, dont un est le max des 4 autres entiers,
il y a plus d'un élément égal au maximum.

Paramètres :
	int scoreN : un des quatre entiers;
	int scoreE : un des quatre entiers;
	int scoreS : un des quatre entiers;
	int scoreW : un des quatre entiers;
	int maxscore : le maximum des quatre entiers précédents.
Retourne :
	bool : "true" s'il existe deux éléments ou plus égaux à maxscore, "false" sinon.

*/

bool detecte_egalite(int scoreN,int scoreE,int scoreS,int scoreW,int maxscore){
	bool res; // booléen indiquant s'il y a une égalité entre deux scores sont égaux au maximum
	if(    (maxscore==scoreN && maxscore==scoreE) || (maxscore==scoreN && maxscore==scoreS) 
		|| (maxscore==scoreN && maxscore==scoreW) || (maxscore==scoreE && maxscore==scoreS) 
		|| (maxscore==scoreE && maxscore==scoreW) || (maxscore==scoreS && maxscore==scoreW))
		res = true;
	else res = false;

	return res;
}


/* Fonction char_vers_direction 

Traduit un caractère 'dir' en la direction correspondante (type direction).

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	char dir : un caractère valant 'n','e','s','w','1','2','3' ou '4'.
Retourne :
	direction : la direction associée à la valeur du caractère 'dir'.
Utilité :
	Cela est réalisé pour bien distinguer les procédures d'analyse du champ de vision et 
	les directions choisies par ces dernières.
Note :
	Les arbres itemtree servent à vérifier laquelle des composantes d'une direction mixte est valide.

*/

direction char_vers_direction(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, char dir){
	direction d;

	if(dir == 'n') d = NORTH;
	else if(dir == 'e') d = EAST;
	else if(dir == 's') d = SOUTH;
	else if(dir == 'w') d = WEST;
	else if(dir == '1'){
		if(estValide(ntree)) d = NORTH;
		else d = WEST;
	}
	else if(dir == '2'){
		if(estValide(ntree)) d = NORTH;
		else d = EAST;
	}
	else if(dir == '3'){
		// au lieu de regarder d'abord si le Sud est valide, on regarde plutôt à l'Ouest
		// pour ne pas faire comme dans les deux cas qui précèdent. Autrement, on risque
		// de créer des oscillatons Nord/Sud.
		if(estValide(wtree)) d = WEST; 
		else d = SOUTH;
	}
	else if(dir == '4'){
		if(estValide(etree)) d = EAST;
		else d = SOUTH;
	}

	return d;
}


/* Fonction fuir 

Choisit la direction la plus sûre pour échapper aux fantômes.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	ghost * GhostTab : le tableau dynamique de type ghost du module principal.
Retourne :
	direction : une direction valide permettant d'échapper au mieux aux fantômes.

*/

direction fuir(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, ghost * GhostTab){
	direction d;

	// séparation en plusieurs cas pour ne pas appeler la procédure detection_danger 
	// sur le mauvais indice de fantôme. En effet, à ce stade, on sait uniquement 
	// qu'il y a un fantôme à moins de 4 cases de Pacman, mais pas lequel !
	if(GhostTab[0].dist < 4 && GhostTab[0].dist >= 0) detection_danger(ntree,etree,stree,wtree,1,&d,GhostTab);

	if(GhostTab[1].dist < 4 && GhostTab[1].dist >= 0) detection_danger(ntree,etree,stree,wtree,2,&d,GhostTab);

	if(GhostTab[2].dist < 4 && GhostTab[2].dist >= 0) detection_danger(ntree,etree,stree,wtree,3,&d,GhostTab);

	if(GhostTab[3].dist < 4 && GhostTab[3].dist >= 0) detection_danger(ntree,etree,stree,wtree,4,&d,GhostTab);

	return d;
}


/* Procédure detection_danger

Cherche la direction dans laquelle GHOSTi a été repéré, et appelle la procédure echappatoire
dans cette direction.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	int ind_fantome : le numéro d'un des 4 fantômes (1<=ind_fantome<=4);
	direction * pDir : un pointeur sur direction d, en charge de mettre à jour la direction d
					   définie dans la fonction fuir.
	ghost * GhostTab : le tableau dynamique de type ghost du module principal.

*/

void detection_danger(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, int ind_fantome, direction *pDir, ghost * GhostTab){
	if(GhostTab[ind_fantome-1].dir == 'n') echappatoire('n',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == 'e') echappatoire('e',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == 's') echappatoire('s',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == 'w') echappatoire('w',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == '1') echappatoire('1',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == '2') echappatoire('2',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == '3') echappatoire('3',pDir,ntree,etree,stree,wtree);
	else if(GhostTab[ind_fantome-1].dir == '4') echappatoire('4',pDir,ntree,etree,stree,wtree);
}


/* Procédure echappatoire 

Détermine une direction de fuite valide sachant dans quelle direction se trouve un fantôme,
et met à jour cette direction dans la fonction fuir.

Paramètres :
	char point_cardinal : un des 4 points cardinaux ('n','e','s' ou 'w') ou une des quatres
						  directions mixtes ('1','2','3' ou '4');
	direction *pDir : un pointeur sur direction d, en charge de mettre à jour la direction d
					  définie dans la fonction fuir;
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal.

*/

void echappatoire(char point_cardinal, direction * pDir, itemtree ntree, itemtree etree, itemtree stree, itemtree wtree){
	
	if(point_cardinal == 'n'){
		if(estValide(stree) && !presence_fantome(stree)) *pDir = SOUTH;
		else if(estValide(wtree) && !presence_fantome(wtree)) *pDir = WEST;
		else if(estValide(etree) && !presence_fantome(etree)) *pDir = EAST;
	}

	else if(point_cardinal == 'e'){
		if(estValide(wtree) && !presence_fantome(wtree)) *pDir = WEST;
		else if(estValide(ntree) && !presence_fantome(ntree)) *pDir = NORTH;
		else if(estValide(stree) && !presence_fantome(stree)) *pDir = SOUTH;
	}

	else if(point_cardinal == 's'){
		if(estValide(ntree) && !presence_fantome(ntree)) *pDir = NORTH;
		else if(estValide(etree) && !presence_fantome(etree)) *pDir = EAST;
		else if(estValide(wtree) && !presence_fantome(wtree)) *pDir = WEST;
	}

	else if(point_cardinal == 'w'){
		if(estValide(etree) && !presence_fantome(etree)) *pDir = EAST;
		else if(estValide(stree) && !presence_fantome(stree)) *pDir = SOUTH;
		else if(estValide(ntree) && !presence_fantome(ntree)) *pDir = NORTH;
	}

	else if(point_cardinal == '1'){ // Nord-Ouest
		if(estValide(stree) && !presence_fantome(stree)) *pDir = SOUTH;
		else if(estValide(etree) && !presence_fantome(etree)) *pDir = EAST;
	}

	else if(point_cardinal == '2'){ // Nord-Est
		if(estValide(stree) && !presence_fantome(stree)) *pDir = SOUTH;
		else if(estValide(wtree) && !presence_fantome(wtree)) *pDir = WEST;
	}

	else if(point_cardinal == '3'){ // Sud-Ouest
		if(estValide(ntree) && !presence_fantome(ntree)) *pDir = NORTH;
		else if(estValide(etree) && !presence_fantome(etree)) *pDir = EAST;
	}

	else if(point_cardinal == '4'){ // Sud-Est
		if(estValide(ntree) && !presence_fantome(ntree)) *pDir = NORTH;
		else if(estValide(wtree) && !presence_fantome(wtree)) *pDir = WEST;
	}

	// sinon, c'est que Pacman était acculé -> il doit mourir
	else *pDir = char_vers_direction(ntree,etree,stree,wtree,point_cardinal);
}


/* Fonction estValide 

Détermine si une direction donnée est empruntable.
Motivation : 
	savoir si le module echappatoire fournit une direction valide et contraint les fonctions 
	explorer et char_vers_direction a choisir une direction valide, ie. une direction qui n'est 
	ni un mur ni la porte des fantômes.
Paramètres :
	itemtree tree : l'arbre de direction dans lequel on regarde si l'on peut aller.
Retourne :
	bool : "false" si la racine de l'arbre entrée est un mur ou la porte des fantômes,
		   "true" sinon.

*/

bool estValide(itemtree tree){
	bool res;
	// si la case devant Pacman (selon l'arbre tree entré) est un mur ou la porte des fantômes, 
	// alors la direction n'est pas valide
	if((tree->item == WALL) || (tree->item == DOOR)) res = false;
	else res = true;

	return res;
}


/* Fonction presence_fantome 

Détermine si un fantôme est présent à moins de 4 cases de Pacman selon une direction associée
à l'arbre itemtree fourni en paramètre.
Paramètres :
	itemtree tree : l'arbre dans lequel on recherche si un fantôme est à proximité de Pacman.
Retourne :
	bool : "true" si un ou plusieurs fantômes sont présents, "false" sinon.
Utilité :
	permet au module echappatoire de choisir une direction permettant d'éviter plusieurs
	fantômes à la fois (contre un seul auparavant).

*/

bool presence_fantome(itemtree tree){
	bool res = false;

	int dist_ghost1 = distance(tree,GHOST1);
	int dist_ghost2 = distance(tree,GHOST2);
	int dist_ghost3 = distance(tree,GHOST3);
	int dist_ghost4 = distance(tree,GHOST4);

	if(dist_ghost1 != -1 && dist_ghost1 < 4) res = true;
	if(dist_ghost2 != -1 && dist_ghost2 < 4) res = true;
	if(dist_ghost3 != -1 && dist_ghost3 < 4) res = true;
	if(dist_ghost4 != -1 && dist_ghost4 < 4) res = true;

	return res;
}


/* Fonction attaquer 

Détermine une direction permettant de chasser un fantôme proche à l'aide du tableau GhostTab
fourni en paramètre.

Pramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	ghost * GhostTab : le tableau dynamique de type ghost du module principal.

Retourne :
	direction : une direction valide permettant de manger un fantôme proche.

*/

direction attaquer(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, ghost * GhostTab){
	direction d;

	// d'après cette structure conditionnelle, on ne chasse qu'un fantôme à la fois
	// elle ne garantit cependant pas que l'on chassera toujours le même
	if(GhostTab[0].dist < 4 && GhostTab[0].dist > 0) d = char_vers_direction(ntree,etree,stree,wtree,GhostTab[0].dir);
	else if(GhostTab[1].dist < 4 && GhostTab[1].dist > 0) d = char_vers_direction(ntree,etree,stree,wtree,GhostTab[1].dir);
	else if(GhostTab[2].dist < 4 && GhostTab[2].dist > 0) d = char_vers_direction(ntree,etree,stree,wtree,GhostTab[2].dir);
	else if(GhostTab[3].dist < 4 && GhostTab[3].dist > 0) d = char_vers_direction(ntree,etree,stree,wtree,GhostTab[3].dir);

	return d;
}


/* Fonction explorer 

Détermine une direction permettant d'effectuer un parcours des grands axes de la carte 
afin que cases encore non explorées entrent dans le champ de vision de Pacman.

Paramètres :
	itemtree ntree : l'arbre northtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree stree : l'arbre southtree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree etree : l'arbre easttree montrant la vision au Nord de Pacman dans le programme principal;
	itemtree wtree : l'arbre westtree montrant la vision au Nord de Pacman dans le programme principal;
	direction lastdirection : la dernière direction empruntée par Pacman.
Retourne :
	direction : une direction valide permettant de continuer un parcours des grands axes de la carte.

*/

direction explorer(itemtree ntree, itemtree etree, itemtree stree, itemtree wtree, direction lastdirection){
	direction d;

	if(lastdirection == NORTH && estValide(ntree)){
		d = lastdirection;
	}

	else if(lastdirection == NORTH && !estValide(ntree)){ // (*1)
		if(estValide(etree)) d = EAST;
		else if(estValide(wtree)) d = WEST;
		else if(estValide(stree)) d = SOUTH;
	}

	else if(lastdirection == EAST && estValide(etree)){
		d = lastdirection;
	}

	else if(lastdirection == EAST && !estValide(etree)){ // (*2)
		if(estValide(ntree)) d = NORTH;
		else if(estValide(stree)) d = SOUTH;
		else if(estValide(wtree)) d = WEST;
	}

	else if(lastdirection == SOUTH && estValide(stree)){
		d = lastdirection;
	}

	else if(lastdirection == SOUTH && !estValide(stree)){
		// on inverse l'ordre de choix de WEST et EAST par rapport à (*1)
		// afin d'éviter des oscillations.
		if(estValide(wtree)) d = WEST;
		else if(estValide(etree)) d = EAST;
		else if(estValide(ntree)) d = NORTH;
	}

	else if(lastdirection == WEST && estValide(wtree)){
		d = lastdirection;
	}

	else if(lastdirection == WEST && !estValide(wtree)){
		// on inverse l'ordre de choix de NORTH et SOUTH par rapport à (*2)
		// afin d'éviter des oscillations.
		if(estValide(stree)) d = SOUTH;
		else if(estValide(ntree)) d = NORTH;
		else if(estValide(etree)) d = EAST;
	}

	return d;
}
