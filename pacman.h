
/*
  Enumeration for the different directions that the compass can indicate
 */
enum compass {NORTH, EAST, SOUTH, WEST};
typedef enum compass direction;

/*
  Tree of map items, used to model what pacman is seeing when looking in the four directions
 */
struct itemnode {char item; struct itemnode * n; struct itemnode * e; struct itemnode * s; struct itemnode * w;};
typedef struct itemnode * itemtree;

/*
  Tree of directions, used to record the moves pacman made since the beginning of the game
 */
struct dirnode {direction d; struct dirnode * n; struct dirnode * e; struct dirnode * s; struct dirnode * w;};
typedef struct dirnode * dirtree;

/*
  The pacman function you must provide. See pacman.c for more details
 */
direction pacman(dirtree, dirtree, itemtree, itemtree, itemtree, itemtree, direction, bool, int);

